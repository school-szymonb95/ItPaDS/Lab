#include "Tasks.h"

int mod(int a, int b)
{
	return (a % b + b) % b;
}

void AllToAllBcRing(int nodeId, int nodes)
{
	int left = mod(nodeId - 1, nodes);
	int right = mod(nodeId + 1, nodes);
	int message = (1000 * nodeId);
	for (int i = 1; i <= nodes - 1; i++)
	{
		if (i <= nodeId)
		{
			Tasks::send(right, message + i - 1);
		}
		else
		{
			Tasks::send(right, message + i);
		}
	}
	std::vector<int> result;
	while (result.size() < nodes - 1)
	{
		int received = Tasks::receive(left);
		if (received % 1000 == nodeId)
		{
			//printf("Node %d obtained: %d\n", nodeId, received);
			result.push_back(received);
		}
		else
		{
			Tasks::send(right, received);
		}
	}
	std::cout << ">>> " << nodeId << ": ";
	for (int message : result)
	{
		printf("%04d, ", message);
	}
	std::cout << " <<<" << std::endl;
}

void Tasks::lab3task3()
{
	AllToAllBcRing(NODE_ID, NODES);
}
