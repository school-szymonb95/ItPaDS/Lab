#include "Tasks.h"

#define SIZE 50000

struct MinResult
{
	int index, value;
	MinResult() : index(-1), value(-1) {}
	MinResult(std::vector<int>& v)
	{
		auto min = std::min_element(v.begin(), v.end());
		index = std::distance(v.begin(), min);
		value = *min;
	}
	MinResult(std::map<int, int> m)
	{
		auto min = std::min_element(m.begin(), m.end(), [](decltype(m)::value_type& l, decltype(m)::value_type& r) -> bool { return l.second < r.second; });
		index = min->first;
		value = min->second;
	}
};

struct NodeIndex
{
	int node, index;
	NodeIndex(int node, int index) : node(node), index(index)
	{
	}
};

void distributeVector(std::vector<int>& vector);
NodeIndex getNodeIndex(int index);


void Tasks::lab5()
{
	std::vector<int> vector;
	int vectorSize = 0;
	if (NODE_ID == 0)
	{
		vector = Tasks::generateVector(SIZE);
		vectorSize = vector.size();
		MinResult m(vector);
		if (LOG_PRIORITY > 1) Tasks::showVector(vector);
		/*for (int i = 0; i < vector.size(); i++)
		{
			std::cout << getNodeIndex(i).node << " ";
		}
		std::cout << std::endl;*/
		distributeVector(vector);
		//if (LOG_PRIORITY > 1) Tasks::showVector(vector);
	}
	else
	{
		int numbers = Tasks::receive(0);
		vector = Tasks::receiveVector(numbers, 0);
		//if (LOG_PRIORITY > 1) Tasks::showVector(vector);
	}


	for (int i = 0; i < SIZE; i++)
	{
		NodeIndex nodeIndex = getNodeIndex(i);
		MinResult localMin;
		if (nodeIndex.node != NODE_ID)
			localMin = MinResult(vector);
		else
		{
			std::vector<int> v(vector.begin() + nodeIndex.index, vector.end());
			localMin = MinResult(v);
		}
		if (nodeIndex.node == NODE_ID)
		{
			std::map<int, int> localMins;
			for (int node = nodeIndex.node; node < NODES; node++)
			{
				if (node == NODE_ID)
				{
					localMins.emplace(node, localMin.value);
				}
				else
				{
					localMins.emplace(node, receive(node));
				}
			}
			//printf("i = %d localMins: ", i);
			//showMap(localMins);
			MinResult globalMin(localMins);
			if (globalMin.index == NODE_ID)
			{
				std::swap(vector[nodeIndex.index], vector[nodeIndex.index + localMin.index]);// printf("i = %d ", i); printf("%d: swap %d with %d\n", NODE_ID, nodeIndex.index, localMin.index);
				sendToAll(globalMin.index);// printf("i = %d ", i); printf("%d: globalMin at: %d.%d\n", NODE_ID, NODE_ID, localMin.index);
			}
			else
			{
				sendToAll(globalMin.index);// printf("i = %d ", i); printf("%d: globalMin at: %d\n", NODE_ID, globalMin.index);
				send(globalMin.index, vector[nodeIndex.index]);// printf("i = %d ", i); printf("%d: sending value %d to replace in %d\n", NODE_ID, vector[nodeIndex.index], globalMin.index);
				vector[nodeIndex.index] = globalMin.value;
			}
		}
		else if (nodeIndex.node < NODE_ID)
		{
			send(nodeIndex.node, localMin.value);
			int minFoundAt = receive(nodeIndex.node);
			if (minFoundAt == NODE_ID)
			{
				int valueToReplace = receive(nodeIndex.node);// printf("i = %d ", i); printf("%d: globalMinIndexReceived from: %d (%d)\n", NODE_ID, nodeIndex.node, valueToReplace);
				vector[localMin.index] = valueToReplace;
			}
		}
		//printf("i = %d ", i);
		//showVector(vector);
	}
	//showVector(vector);


	if (NODE_ID == 0)
	{
		for (int node = 1; node < NODES; node++)
		{
			int m = 0;
			while (m != -1)
				m = receive(node);
			int numbersToReceive = receive(node);
			std::vector<int> receivedVector = receiveVector(numbersToReceive, node);
			vector.insert(vector.end(), receivedVector.begin(), receivedVector.end());
		}
		//showVector(vector);

	}
	else
	{
		send(0, -1);
		send(0, vector.size());
		sendVector(vector, 0);
	}

}

void distributeVector(std::vector<int>& vector)
{
	int numbers = vector.size();
	int numbersPerNode = numbers / NODES;
	int remainder = numbers % NODES;
	int zeroNodeNumbers = (remainder - 1 >= 0) ? numbersPerNode + 1 : numbersPerNode;
	int currentNumber = zeroNodeNumbers;
	for (int node = 1; node < NODES; node++)
	{
		int numbersToSend = (node <= remainder - 1) ? numbersPerNode + 1 : numbersPerNode;
		if (currentNumber < vector.size())
		{
			auto begin = vector.begin() + currentNumber;
			//int endIndex = numbersToSend < vector.size() ? numbersToSend : vector.size() - 1;
			auto end = begin + numbersToSend;
			std::vector<int> vectorToSend(begin, end);
			Tasks::send(node, vectorToSend.size());
			Tasks::sendVector(vectorToSend, node);
		}
		else
		{
			Tasks::send(node, 0);
		}
		currentNumber += numbersToSend;
	}
	vector.erase(vector.begin() + zeroNodeNumbers, vector.end());
}

NodeIndex getNodeIndex(int index)
{
	int numbersPerNode = SIZE / NODES;
	int remainder = SIZE % NODES;
	int currentNumber = 0;
	int node = 0;
	while (index >= 0)
	{
		int numbersToSend = (node <= remainder - 1) ? numbersPerNode + 1 : numbersPerNode;
		if (index < numbersToSend)
			return NodeIndex(node, index);
		index -= numbersToSend;
		node++;

	}
	return NodeIndex(-1, -1);
}