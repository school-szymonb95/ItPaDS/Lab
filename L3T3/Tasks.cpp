#include "Tasks.h"

void Tasks::send(int destination, int message)
{
	MPI_Send(&message, 1, MPI_INT, destination, 0, MPI_COMM_WORLD);
	if (LOG_PRIORITY > 2) printf("%d --%02d-> %d\n", NODE_ID, message, destination);
}

void Tasks::sendToAll(int message)
{
	for (int node = 0; node < NODES; node++)
	{
		if (node != NODE_ID)
			send(node, message);
	}
}

int Tasks::receive(int source)
{
	int message;
	//printf("%d <-..-- %d\n", NODE_ID, source);
	MPI_Recv(&message, 1, MPI_INT, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	if (LOG_PRIORITY > 2) printf("%d <-%02d-- %d\n", NODE_ID, message, source);
	return message;
}

int Tasks::getCurrentNode()
{
	int nodeId = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &nodeId);
	return nodeId;
}

int Tasks::getNodesNumber()
{
	int nodes = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &nodes);
	return nodes;
}

std::vector<int> Tasks::generateVector(int size)
{
	std::vector<int> vector(size, 0);
	for (int& number : vector)
	{
		number = randomize();
	}
	return vector;
}

int Tasks::randomize()
{
	return rand() % 50000 + 1;
}

std::vector<int> Tasks::receiveVector(int rows, int source)
{
	std::vector<int> v(rows, 0);
	for (int& number : v)
	{
		number = Tasks::receive(source);
	}
	return v;
}

void Tasks::sendVector(std::vector<int>& v, int destination)
{
	for (int number : v)
	{
		Tasks::send(destination, number);
	}
}

void Tasks::showVector(std::vector<int>& v)
{
	std::cout << NODE_ID << ": ";
	for (int number : v)
	{
		std::cout << number << " ";
	}
	std::cout << std::endl;
}

void Tasks::showMap(std::map<int, int>& m)
{
	printf("%d: ", NODE_ID);
	for (auto o : m)
	{
		printf("%d(%d) ", o.first, o.second);
	}
	printf("\n");
}
