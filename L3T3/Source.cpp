#include "Tasks.h"

int main(int argc, char **argv)
{
	srand(time(NULL));
	MPI_Init(&argc, &argv);
	double startTime = MPI_Wtime();

	Tasks::lab5();

	double stopTime = MPI_Wtime();
	printf("%d: Elapsed time is %f\n", NODE_ID, stopTime - startTime);
	MPI_Finalize();
	return 0;
}
