#include "Tasks.h"

#define SIZE 5000

using Matrix = std::vector< std::vector<int> >;

Matrix generateMatrix();
void showMatrix(Matrix& m);
std::vector<int> matrixVector(Matrix& a, std::vector<int>& x);
void distributeRows(Matrix& m, std::vector<int> v);
Matrix receiveMatrix(int rows, int rowsLength);
std::vector<int> matrixVectorMultiplication(std::vector<int> v, Matrix m);

void Tasks::lab4()
{
	if (NODE_ID == 0) {
		Matrix m = generateMatrix();
		if (LOG_PRIORITY > 0) printf("0: Matrix\n");
		if (LOG_PRIORITY > 0) showMatrix(m);
		if (LOG_PRIORITY > 0) printf("0: Vector: ");
		std::vector<int> v = Tasks::generateVector(SIZE);
		if (LOG_PRIORITY > 0) showVector(v);
		distributeRows(m, v);
		if (LOG_PRIORITY > 1) printf("0: Matrix\n");
		if (LOG_PRIORITY > 1) showMatrix(m);
		
		std::vector<int> result = matrixVectorMultiplication(v, m);
		if (LOG_PRIORITY > 1) printf("0: Result: ");
		if (LOG_PRIORITY > 1) showVector(result);
		for (int i = 1; i < NODES; i++) {
			int vectorSize = Tasks::receive(i);
			std::vector<int> resultFromSlave = receiveVector(vectorSize, i);
			result.insert(result.end(), resultFromSlave.begin(), resultFromSlave.end());
		}
		if (LOG_PRIORITY > 0) printf("\n\n\n0: Final Result: ");
		if (LOG_PRIORITY > 0) showVector(result);
	}
	else
	{
		int rows = receive(0);
		int rowsLength = receive(0);
		Matrix m = receiveMatrix(rows, rowsLength);
		std::vector<int> v = receiveVector(rowsLength, 0);
		if (LOG_PRIORITY > 1) printf("%d: Matrix\n", NODE_ID);
		if (LOG_PRIORITY > 1) showMatrix(m);
		if (LOG_PRIORITY > 1) printf("%d: Vector: ", NODE_ID);
		if (LOG_PRIORITY > 1) showVector(v);

		std::vector<int> result = matrixVectorMultiplication(v, m);
		if (LOG_PRIORITY > 1) printf("%d: Result: ", NODE_ID);
		if (LOG_PRIORITY > 1) showVector(result);
		Tasks::send(0, result.size());
		sendVector(result, 0);
	}

}

Matrix generateMatrix()
{
	Matrix matrix(SIZE, std::vector<int>(SIZE, 0));
	for (auto& row : matrix) {
		for (int& number : row)
		{
			number = Tasks::randomize();
		}
	}
	return matrix;
}

void showMatrix(Matrix & m)
{
	for (auto row : m)
	{
		printf("%d: ", NODE_ID);
		for (int number : row)
		{
			std::cout << number << " ";
		}
		std::cout << std::endl;
	}
}

std::vector<int> matrixVector(Matrix& a, std::vector<int>& x)
{
	return std::vector<int>();
}

void distributeRows(Matrix& m, std::vector<int> v)
{
	int rows = m.size();
	int rowsPerNode = rows / NODES;
	int remainder = rows % NODES;
	int currentRow = (0 <= remainder - 1) ? rowsPerNode + 1 : rowsPerNode;
	int zeroNodeRows = currentRow;
	for (int i = 1; i < NODES; i++)
	{
		int rowsToSend = (i <= remainder - 1) ? rowsPerNode + 1 : rowsPerNode;
		Tasks::send(i, rowsToSend);
		Tasks::send(i, rows);
		for (int j = currentRow; j < currentRow + rowsToSend; j++)
		{
			for (int number : m[j])
			{
				Tasks::send(i, number);
			}
		}
		Tasks::sendVector(v, i);
		currentRow += rowsToSend;
	}
	m.erase(m.begin() + zeroNodeRows, m.end());
}

Matrix receiveMatrix(int rows, int rowsLength)
{
	Matrix m(rows, std::vector<int>(rowsLength, 0));
	for (auto& row : m)
	{
		for (int& number : row)
		{
			number = Tasks::receive(0);
		}
	}
	return m;
}

std::vector<int> matrixVectorMultiplication(std::vector<int> v, Matrix m)
{
	std::vector<int> result(m.size(), 0);
	for (int i = 0; i < result.size(); i++)
	{
		for (int j = 0; j < v.size(); j++)
		{
			result[i] += m[i][j] * v[j];
		}
	}
	return result;
}
