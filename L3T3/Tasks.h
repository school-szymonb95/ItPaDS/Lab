#pragma once
#include <stdio.h>
#include <mpi.h>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <map>
#include <queue>
#include <iostream>

#define NODE_ID Tasks::getCurrentNode()
#define NODES Tasks::getNodesNumber()
#define LOG_PRIORITY 0

class Tasks
{
public:
	static void send(int destination, int message);
	static void sendToAll(int message);
	static int receive(int source);
	static void lab3task3();
	static void lab4();
	static void lab5();
	static int getCurrentNode();
	static int getNodesNumber();
	static int randomize();
	static std::vector<int> generateVector(int size);
	static std::vector<int> receiveVector(int rows, int source);
	static void sendVector(std::vector<int>& v, int destination);
	static void showVector(std::vector<int>& v);
	static void showMap(std::map<int, int>& m);
};